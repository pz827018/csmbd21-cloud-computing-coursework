# CSMBD21 Cloud Computing Coursework

Cloud Computing Prototype Solution to find passenger with highest number of flights

This repository contains two .py file one for mapper and reducer, one .csv for data, and one output.txt

1. map_func.py

2. reduce_func.py

3. AComp_Passenger_data_no_error.csv

4. output.txt

To get output.txt run following in Terminal

type AComp_Passenger_data_no_error.csv|python map_func.py|sort|python reduce_func.py>output.txt
