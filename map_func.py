import sys
import multiprocessing as mp

def mapper(array):
    # whitespace at the start and the end are removed
    array = array.strip()
    # array is split into fields
    passenger_id, _, _, _, _, _ = array.split(',')
    # emit (passenger_id, 1) key-value pair
    return (passenger_id, 1)

if __name__ == '__main__':
    # take input from STDIN (standard input)
    with sys.stdin as func:
        mapper_in = func.readlines()
        with mp.Pool(processes=mp.cpu_count()) as pool:
            mapper_out = pool.map(mapper, mapper_in, chunksize=int(len(mapper_in)/mp.cpu_count()))
            for key, value in mapper_out:
                # write the results to STDOUT
                print('%s\t%s' % (key, value))
