import sys
import multiprocessing as mp
from map_func import mapper

def reducer(input_list):
    # defining intial values for variables
    current_passenger = None
    count = 0
    passenger = None
    m_count = 0
    m_passenger = None

    # running through each line of input value from mapper
    for array in input_list:
        # stripping leading and trailing whitespace
        array = array.strip()

        # Parsing the input value recieved from mapper
        key, value = array.split('\t', 1)

        # taking a count of total number of flights for each passenger
        if key == current_passenger:
            count += int(value)
        else:
            if current_passenger:
                # pass passenger's flight count to STDOUT
                print('%s\t%s' % (current_passenger, count))
                # update maximum counter and current passenger if required
                if count > m_count:
                    m_count = count
                    m_passenger = current_passenger
            # reset count and current passenger to next passenger
            count = int(value)
            current_passenger = key

    # output count of final passenger's flights if required
    if current_passenger == passenger:
        print('%s\t%s' % (current_passenger, count))
        if count > m_count:
            m_count = count
            m_passenger = current_passenger

    # output the passenger_id with highest number of flights
    print('\nHighest number of flights taken by the passenger: %s' % m_passenger)
    print('Count of flights taken by the passenger: %s' % m_count)

if __name__ == '__main__':
    # input comes from STDIN (standard input)
    reducer_in = sys.stdin.readlines()

    # use multiprocessing pool to run reducer function on input
    with mp.Pool(processes=mp.cpu_count()) as pool:
        pool.map(reducer, [reducer_in], chunksize=int(len(reducer_in)/mp.cpu_count()))
